//
//  Style.swift
//  Exchange
//
//  Created by Slava Bulgakov on 12/11/15.
//  Copyright © 2015 Slava Bulgakov. All rights reserved.
//

import UIKit

class Style {
    var line: CGFloat?
    var spacing: Double?
    var font: UIFont?
    var color: UIColor?
    
    init(line: CGFloat?, spacing: Double?, color: UIColor? = nil) {
        self.line = line
        self.spacing = spacing
        self.color = color
    }
    
    init(font: UIFont) {
        self.font = font
    }
}

