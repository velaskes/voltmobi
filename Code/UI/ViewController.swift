//
//  ViewController.swift
//  Exchange
//
//  Created by Slava Bulgakov on 11/11/15.
//  Copyright © 2015 Slava Bulgakov. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var menuView: UIView!
    @IBOutlet weak var bottomMenuConstraint: NSLayoutConstraint!
    @IBOutlet weak var menuItem1: MenuItem!
    @IBOutlet weak var menuItem2: MenuItem!
    @IBOutlet weak var menuItem3: MenuItem!
    @IBOutlet weak var menuItem4: MenuItem!
    @IBOutlet weak var menuItem5: MenuItem!
    @IBOutlet weak var menuItem6: MenuItem!
    @IBOutlet weak var directionLabel: StyleLabel!
    @IBOutlet weak var valueLabel: StyleLabel!
    @IBOutlet weak var descriptionLabel: StyleLabel!
    @IBOutlet weak var updateLabel: StyleLabel!
    @IBOutlet weak var mainView: UIView!
    
    private var _rates = [Rate(), Rate()]
    private var _currentDirection: Direction = Direction(from: .USD, to: .RUR)
    private var _menuItems: [MenuItem]!
    private var _hasData = false
    private var _dateFormatter: NSDateFormatter!
    private var _lastUpdatedDate: NSDate!
    private var _textsGenerator: TextsGenerator!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        _textsGenerator = TextsGenerator()
        
        menuItem1.direction = Direction(from: .USD, to: .RUR)
        menuItem2.direction = Direction(from: .USD, to: .EUR)
        menuItem3.direction = Direction(from: .EUR, to: .RUR)
        menuItem4.direction = Direction(from: .EUR, to: .USD)
        menuItem5.direction = Direction(from: .RUR, to: .USD)
        menuItem6.direction = Direction(from: .RUR, to: .EUR)
        
        _menuItems = [menuItem1, menuItem2, menuItem3, menuItem4, menuItem5, menuItem6]
        self.bottomMenuConstraint.constant = Constants.bottomMenuConstraintValue
       
        directionLabel.styles = [Style(line: nil, spacing: Constants.directionLabelSpacing)]
        valueLabel.styles = [Style(line: nil, spacing: Constants.valueLabelSpacing)]
        descriptionLabel.styles = [
            Style(line: Constants.descriptionLabelLine, spacing: nil, color:Palette.yellow),
            Style(line: Constants.descriptionLabelLine, spacing: nil, color:Palette.green),
            Style(line: Constants.descriptionLabelLine, spacing: nil, color:Palette.red)
        ]
        updateLabel.styles = [Style(line: nil, spacing: Constants.updateLabelSpacing)]
        _menuItems.forEach {
            $0.styles = [Style(font: UIFont(name: Constants.latoRegular, size: Constants.menuItemFontSize)!), Style(font: UIFont(name: Constants.latoBlack, size: Constants.menuItemFontSize)!)]
        }
        
        _updateRateView()
        _loadData()
    }
    
    @IBAction func menuButtonTap(sender: UIButton) {
        _showBottomMenu(true)
    }
    
    @IBAction func menuItemTap(sender: MenuItem) {
        _showBottomMenu(false)
        _currentDirection = sender.direction
        _updateRateView()
    }
    
    // MARK:- Private methods
    private func _showBottomMenu(show: Bool) {
        self.bottomMenuConstraint.constant = show ? 0 : Constants.bottomMenuConstraintValue
        UIView.animateWithDuration(Constants.animationDuration, delay: 0, options: UIViewAnimationOptions.CurveEaseInOut, animations: { () -> Void in
            self.view.layoutIfNeeded()
            }, completion: nil)
    }
    
    private func _yesterday() -> String {
        let calendar = NSCalendar.currentCalendar()
        let components = calendar.components(NSCalendarUnit.Hour, fromDate: NSDate())
        components.hour = Constants.yesterdayHoursBack
        let yesterday = calendar.dateByAddingComponents(components, toDate: _rates[0].date, options: [])
        if _dateFormatter == nil {
            _dateFormatter = NSDateFormatter()
            _dateFormatter!.dateFormat = Constants.dateFormat
        }
        return _dateFormatter!.stringFromDate(yesterday!)
    }
    
    private func _loadData() {
        mainView.hidden = true
        let alert = UIAlertView(title: NSLocalizedString(Constants.pleaseWait, comment: ""), message: nil, delegate: nil, cancelButtonTitle: nil)
        alert.show()
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            var index = 0
            for rate in self._rates {
                let urlEnd = index == 0 ? Constants.latest : self._yesterday()
                let data = NSData(contentsOfURL: NSURL(string: Constants.urlBase + urlEnd)!)
                let json = try! NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions(rawValue: 0))
                    as! [String: AnyObject]
                rate.fill(json)
                index++
            }
            dispatch_async(dispatch_get_main_queue(), {
                self.mainView.hidden = false
                self._lastUpdatedDate = NSDate()
                self._hasData = true
                alert.dismissWithClickedButtonIndex(0, animated: true)
                self._updateRateView()
            })
        }
    }
    
    func _updateRateView() {
        _menuItems.forEach {
            $0.selected = ($0.direction.from == _currentDirection.from && $0.direction.to == _currentDirection.to)
        }
        
        directionLabel.text = TextsGenerator.directionToString(_currentDirection)
        if let lastUpdatedDate = self._lastUpdatedDate {
            _textsGenerator.fillUpdateLabel(updateLabel, date: lastUpdatedDate)
        }
        
        if _hasData {
            let todayRate = self._rates[0].rate(self._currentDirection)
            let yesterdayRate = self._rates[1].rate(self._currentDirection)
            _textsGenerator.fillValueLabel(valueLabel, value: todayRate)
            
            let percents = Int(round(((todayRate - yesterdayRate) / yesterdayRate) * 100.0))
            _textsGenerator.fillDescriptionTextLabel(self.descriptionLabel, percents: percents, currentCurrency: _currentDirection.from)
        }
    }
}

