//
//  StyleLabel.swift
//  Exchange
//
//  Created by Slava Bulgakov on 12/11/15.
//  Copyright © 2015 Slava Bulgakov. All rights reserved.
//

import UIKit

class StyleLabel: UILabel {
    enum State: Int {
        case Neutral = 0
        case Positive = 1
        case Negative = 2
    }
    
    var currentState = State.Neutral
    var styles: [Style]! {
        didSet {
            _update()
        }
    }
    var style: Style! {
        get {
            return styles[currentState.rawValue]
        }
    }
    
    override var text: String? {
        didSet {
            _update()
        }
    }
    
    // MARK:- Private methods
    private func _update() {
        if let text = self.text, let style = self.style {
            let attributedString = NSMutableAttributedString(string: text)
            if let line = style.line {
                let ps = NSMutableParagraphStyle()
                ps.alignment = NSTextAlignment.Center
                ps.lineSpacing = line
                attributedString.addAttribute(NSParagraphStyleAttributeName, value: ps, range: NSMakeRange(0, attributedString.length))
            }
            if let spacing = style.spacing {
                attributedString.addAttribute(NSKernAttributeName, value: spacing, range: NSMakeRange(0, attributedString.length))
            }
            if let color = style.color {
                attributedString.addAttribute(NSForegroundColorAttributeName, value: color, range: NSMakeRange(0, attributedString.length))
            }
            self.attributedText = attributedString
        }
    }
    
    
}