//
//  MenuItem.swift
//  Exchange
//
//  Created by Slava Bulgakov on 12/11/15.
//  Copyright © 2015 Slava Bulgakov. All rights reserved.
//

import Foundation
import UIKit

class MenuItem: UIButton {
    var direction: Direction! {
        didSet {
            self.titleLabel?.text = TextsGenerator.directionToString(direction)
        }
    }
    
    var currentStyleIndex = 0
    var styles: [Style]! {
        didSet {
            _update()
        }
    }
    var style: Style! {
        get {
            if styles.count > currentStyleIndex {
                return styles[currentStyleIndex]
            } else {
                return nil
            }
        }
    }
    
    override var selected: Bool {
        didSet {
            currentStyleIndex = selected ? 1 : 0
            _update()
        }
    }
    
    private func _update() {
        if let style = self.style {
            self.titleLabel?.font = style.font
        }
    }
}