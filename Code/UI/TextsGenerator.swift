//
//  TextsGenerator.swift
//  Exchange
//
//  Created by Sviatoslav Bulgakov on 13/11/15.
//  Copyright © 2015 Slava Bulgakov. All rights reserved.
//

import Foundation

class TextsGenerator {
    private var _currencyFormatter: NSNumberFormatter
    private var _timeFormatter: NSDateFormatter
    
    init() {
        _timeFormatter = NSDateFormatter()
        _timeFormatter.dateFormat = Constants.timeFormat
        
        _currencyFormatter = NSNumberFormatter()
    }
    
    func fillUpdateLabel(label: StyleLabel, date: NSDate) {
        label.text = NSLocalizedString(Constants.updated, comment: "") + " " + _timeString(date)
    }
    
    func fillValueLabel(label: StyleLabel, value: Double) {
        label.text = _currencyString(value)
    }
    
    func fillDescriptionTextLabel(label: StyleLabel, percents: Int, currentCurrency: CurrencyType) {
        let currencyName = NSLocalizedString(currentCurrency.rawValue, comment: "")
        var by = ""
        if percents < 0 {
            by = NSLocalizedString(Constants.fellBy, comment: "")
            label.currentState = .Negative
        } else if percents > 0 {
            by = NSLocalizedString(Constants.roseBy, comment: "")
            label.currentState = .Positive
        } else {
            label.currentState = .Neutral
        }
        let positivePercents = abs(percents)
        let description = NSLocalizedString(Constants.description, comment: "")
        let notChanged = NSLocalizedString(Constants.notChanged, comment: "")
        let localizedPercents = NSString.localizedStringWithFormat(NSLocalizedString(Constants.dPercent, comment: ""), positivePercents) as String
        let endString = positivePercents == 0 ? notChanged : by + " " + localizedPercents
        label.text = "\(description) \(currencyName) \n\(endString)"
    }
    
    static func directionToString(direction: Direction) -> String {
        return "\(direction.from.rawValue) → \(direction.to.rawValue)"
    }
    
    // MARK:- Private methods
    private func _timeString(date: NSDate) -> String {
        return _timeFormatter.stringFromDate(date)
    }
    
    private func _currencyString(value: Double) -> String {
        _currencyFormatter.locale = NSLocale.currentLocale()
        _currencyFormatter.numberStyle = NSNumberFormatterStyle.DecimalStyle
        _currencyFormatter.minimumFractionDigits = Constants.fractionDigits
        _currencyFormatter.maximumFractionDigits = Constants.fractionDigits
        let formattedAmountSting = _currencyFormatter.stringFromNumber(value)
        return formattedAmountSting!
    }
}