//
//  Rate.swift
//  Exchange
//
//  Created by Slava Bulgakov on 12/11/15.
//  Copyright © 2015 Slava Bulgakov. All rights reserved.
//

import Foundation

class Rate {
    var rur: Double = 0
    var usd: Double = 0
    var date: NSDate!
    
    private let _dateFormatter: NSDateFormatter
    
    init() {
        _dateFormatter = NSDateFormatter()
        _dateFormatter.dateFormat = Constants.dateFormat
    }
    
    func fill(json: [String: AnyObject]) {
        let rates = json[Constants.rates] as! [String: Double]
        let stringDate = json[Constants.date] as! String
        rur = rates[Constants.rub]!
        usd = rates[Constants.usd]!
        date = _dateFormatter.dateFromString(stringDate)
    }
    
    func rate(direction: Direction) -> Double {
        var values: [Double] = []
        var index = 0
        for currency in direction.array() {
            var value: Double = 0
            switch currency {
            case .USD:
                value = usd
                break
                
            case .RUR:
                value = rur
                break
                
            case .EUR:
                value = 1
                break
            }
            values.append(value)
            index++
        }
        
        return values[1] / values[0]
    }
}