//
//  Direction.swift
//  Exchange
//
//  Created by Slava Bulgakov on 12/11/15.
//  Copyright © 2015 Slava Bulgakov. All rights reserved.
//

import Foundation

class Direction {
    var from: CurrencyType
    var to: CurrencyType
    
    init(from: CurrencyType, to: CurrencyType) {
        self.from = from;
        self.to = to
    }
    
    func array() -> [CurrencyType] {
        return [from, to]
    }
}