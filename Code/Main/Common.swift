//
//  Constants.swift
//  Exchange
//
//  Created by Slava Bulgakov on 12/11/15.
//  Copyright © 2015 Slava Bulgakov. All rights reserved.
//

import Foundation
import UIKit

class Constants {
    static let urlBase = "https://api.fixer.io/"
    static let dateFormat = "yyyy-MM-dd"
    static let timeFormat = "HH:mm"
    static let latest = "latest"
    static let rates = "rates"
    static let date = "date"
    
    static let animationDuration = 0.4
    static let fractionDigits = 3
    static let menuItemFontSize = CGFloat(14)
    static let bottomMenuConstraintValue = CGFloat(-266)
    static let directionLabelSpacing = 1.0
    static let valueLabelSpacing = -4.0
    static let descriptionLabelLine = CGFloat(1.2)
    static let updateLabelSpacing = 2.0
    static let yesterdayHoursBack = -24
    
    static let rub = "RUB"
    static let usd = "USD"
    
    static let latoRegular = "Lato-Regular"
    static let latoBlack = "Lato-Black"
    
    static let pleaseWait = "PLEASE_WAIT"
    static let fellBy = "FELL_BY"
    static let roseBy = "ROSE_BY"
    static let description = "DESCRIPTION"
    static let updated = "UPDATED"
    static let notChanged = "NOT_CHANGED"
    static let dPercent = "%d percent"
}

extension UIColor {
    static func hexColor(colorCode: String, alpha: Float = 1.0) -> UIColor {
        let scanner = NSScanner(string:colorCode)
        var color:UInt32 = 0;
        scanner.scanHexInt(&color)
        
        let mask = 0x000000FF
        let r = CGFloat(Float(Int(color >> 16) & mask)/255.0)
        let g = CGFloat(Float(Int(color >> 8) & mask)/255.0)
        let b = CGFloat(Float(Int(color) & mask)/255.0)
        
        return UIColor(red: r, green: g, blue: b, alpha: CGFloat(alpha))
    }
}

class Palette {
    static let green = UIColor.hexColor("7ed321")
    static let red = UIColor.hexColor("D00C1A")
    static let yellow = UIColor.hexColor("FFD117")
}

enum CurrencyType: String {
    case USD = "USD"
    case EUR = "EUR"
    case RUR = "RUR"
}