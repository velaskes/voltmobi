//
//  AppDelegate.swift
//  Exchange
//
//  Created by Slava Bulgakov on 11/11/15.
//  Copyright © 2015 Slava Bulgakov. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        return true
    }
}

